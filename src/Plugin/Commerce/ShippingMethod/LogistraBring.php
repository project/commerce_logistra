<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_logistra\CargonizerAPI;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Revised Bring products.
 *
 * For legacy products (starting with bring_ prefix) use LogistraBringLegacy.
 *
 * @CommerceShippingMethod(
 *  id = "commerce_logistra_bring",
 *  label = @Translation("Bring (Logistra)"),
 *  services = {
 *    "bring2_abonnement" = "Abonnementstransport",
 *    "bring2_express_next_day" = "Ekspress neste dag",
 *    "bring2_business_delivery_7" = "FØR 07:00 til bedrift",
 *    "bring2_small_parcel_a_no_rfid" = "Pakke i postkassen",
 *    "bring2_small_parcel_a" = "Pakke i postkassen med RFID",
 *    "bring2_home_delivery_parcel" = "Pakke levert hjem",
 *    "bring2_business_parcel" = "Pakke til bedrift",
 *    "bring2_parcel_pickup_point" = "Pakke til hentested",
 *    "bring2_business_pallet" = "Pall til bedrift",
 *    "bring2_business_partload" = "Partigods til bedrift",
 *    "bring2_pum" = "PUM (Personlig utlevering mottakingsbevis)",
 *    "bring2_rek" = "Rekommandert brev",
 *    "bring2_rek_pp" = "Rekommandert brev PP",
 *    "bring2_rek_utl" = "Rekommandert brev Utland",
 *    "bring2_rek_utl_pp" = "Rekommandert brev Utland PP",
 *    "bring2_return_express" = "Retur ekspress",
 *    "bring2_return_pickup_point" = "Retur fra hentested",
 *    "bring2_return_business_parcel" = "Retur pakke fra bedrift",
 *    "bring2_return_small_parcel" = "Retur Pakke i postkassen",
 *    "bring2_return_business_groupage" = "Retur Stykkgods fra bedrift",
 *    "bring2_business_groupage" = "Stykkgods til bedrift",
 *    "bring2_termo_transport" = "Termo",
 *  }
 * )
 */
class LogistraBring extends LogistraBase implements SupportsTrackingInterface {

  /**
   * Adding Tracking URL pattern here which is utilized in base class.
   *
   * @see \Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod\LogistraBase::getTrackingUrl()
   */
  const TRACKING_URL = "https://sporing.bring.no/sporing/%s";

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager, $entity_type_manager);
    $this->carrierServices = [
      'bring2_0_4c' => '0 - 4 C',
      'bring2_notification' => 'Advisering',
      'bring2_age_check' => 'Alderskontroll',
      'bring2_cash_on_delivery' => 'Bet. ved utlev. (COD)',
      'bring2_extra_pickup_location' => 'Ekstra hentested',
      'bring2_extra_delivery_location' => 'Ekstra leveringssted',
      'bring2_extra_work' => 'Ekstraarbeid',
      'bring2_one_delivery_attempt_then_pickup_point' => 'Ett utleveringsforsøk, så hentested',
      'bring2_fish_and_seafood' => 'Fisk og sjømat 0 til +4C',
      'bring2_simplified_delivery' => 'Forenklet utlevering',
      'bring2_format_maxi' => 'Format Maxi',
      'bring2_format_small' => 'Format Små',
      'bring2_format_big' => 'Format Store',
      'bring2_frost_free' => 'Frostfritt',
      'bring2_pickup_at_terminal' => 'Henting på terminal',
      'bring2_pickup_mailbox' => 'Henting i Postkasse',
      'bring2_id_check' => 'ID-kontrakt',
      'bring2_cooled_4' => 'Kjølegods + 4C',
      'bring2_copy_of_signed_consignment_note' => 'Kopi av signert fraktbrev',
      'bring2_krav_til_pass' => 'Krav til Pass',
      'bring2_loading_with_tailgate' => 'Lasting med løftelem',
      'bring2_proof_of_identity_required' => 'Legitimasjonsplikt',
      'bring2_equipment_rental' => 'Leie av utstyr',
      'bring2_delivery_date' => 'Leveringsdato',
      'bring2_delivery_at_terminal' => 'Levert på terminal',
      'bring2_delivery_at_terminal_fish' => 'Levert på terminal fisk',
      'bring2_delivered_designated_location' => 'Levert på anvist plass',
      'bring2_labelling' => 'Merking',
      'bring2_minus18' => 'Minus 18C (dypfryst)',
      'bring2_reloading_whole_load' => 'Omlasting hele lass',
      'bring2_pallet_handling' => 'Pallehåndtering',
      'bring2_personal_delivery' => 'Personlig utlevering',
      'bring2_pickup' => 'Pick-up',
      'bring2_pickup_order' => 'Pickup Order',
      'bring2_picking_and_distribution_fresh_fish' => 'Plukk og fordeling fersk fisk',
      'bring2_picking_and_distribution_of_other_goods' => 'Plukk og fordeling øvrig gods',
      'bring2_delivery_to_door_handle' => 'Pose på døren',
      'bring2_bag_as_packaging_material' => 'Pose som emballasje',
      'bring2_driver_notifies_consignee' => 'Sjåfør ringer',
      'bring2_social_check' => 'Sosial kontroll',
      'bring2_cooled_6' => 'Svalegods + 6 C',
      'bring2_cooled_between_5_8' => 'Svalegods +5C til +8C',
      'bring2_temperature_records' => 'Temperaturlogg',
      'bring2_thermo_transport' => 'Termo',
      'bring2_time_window_pickup' => 'Tidsvindu for henting',
      'bring2_time_window_delivery' => 'Tidsvindu for levering',
      'bring2_to_recycling' => 'Til gjenvinning',
      'bring2_optional_insurance' => 'Tilleggsforsikring',
      'bring2_two_delivery_attempts_then_pickup_point' => 'To utleveringsforsøk, så hentested',
      'bring2_two_delivery_attempts_then_return' => 'To utleveringsforsøk, så retur',
      'bring2_dry_groceries' => 'Tørt/kolonial',
      'bring2_saturday_delivery' => 'Utlevering lørdag',
      'bring2_delivery_evening' => 'Utlevering på kveld',
      'bring2_printout_additional_consignment_note' => 'Utskrift av ekstra fraktbrev',
      'bring2_print_out_consignment_note' => 'Utskrift av fraktbrev',
      'bring2_choice_of_pickup_point' => 'Valgfritt hentested',
      'bring2_heated_10' => 'Varmegods +10C',
      'bring2_heated_15' => 'Varmegods +15C',
      'bring2_heated_over_18' => 'Varmegods +18C',
      'bring2_heated_over_9' => 'Varmegods over +9C',
      'bring2_notification_by_letter' => 'Varsling per brev',
      'bring2_e_notification_to_consignor' => 'Varsling til avsender om utlevering',
      'bring2_electronic_notification_to_a_third_party' => 'Varsling til tredjepart',
    ];
  }

}
