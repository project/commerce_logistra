<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_logistra\Entity\CargonizerProfileInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use zaporylie\Cargonizer\ConsignmentPrinter;

/**
 * Base class for all Logistra related Shipping Method plugins.
 */
abstract class LogistraBase extends ShippingMethodBase implements LogistraInterface {

  /**
   * The Cargonizer API.
   *
   * @var \Drupal\commerce_logistra\CargonizerAPI
   */
  protected $cargonizerAPI;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cargonizer Profile selected for this shipping method.
   *
   * @var \Drupal\commerce_logistra\Entity\CargonizerProfileInterface
   */
  protected $cargonizerProfile;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Assoc list of additional services provided by the Carrier.
   *
   * @var array
   */
  protected $carrierServices;

  /**
   * Constructs a new FlatRate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->entityTypeManager = $entity_type_manager;
    if (!empty($configuration['cargonizer_profile'])) {
      $this->cargonizerProfile = $this
        ->entityTypeManager
        ->getStorage('cargonizer_profile')
        ->load($configuration['cargonizer_profile']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $object = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('entity_type.manager')
    );
    $object->cargonizerAPI = $container->get('commerce_logistra.cargonizer');
    $object->httpClient = $container->get('http_client');
    return $object;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration['cargonizer_profile'] = NULL;
    $configuration['carrier_services'] = [];
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // @todo: Ajaxify this part.
    if ($this->cargonizerProfile instanceof CargonizerProfileInterface && $this->cargonizerProfile->get('transport_agreement_id')) {
      $agreements = $this->cargonizerAPI->getAgreements($this->cargonizerProfile);
      /** @var \zaporylie\Cargonizer\Data\TransportAgreement $agreement */
      foreach ($agreements as $agreement) {
        if ($agreement->getId() == $this->cargonizerProfile->get('transport_agreement_id')) {
          $this->services = array_intersect_key($this->services,
            iterator_to_array($agreement->getProducts()));
        }
      }
    }

    $input = $form_state->getUserInput();
    $values = NestedArray::getValue($input, $form['#parents']);
    $form['services']['#options'] = array_intersect_key($form['services']['#options'], $this->services);

    // @todo: Limit number of services available based on transport agreement.
    if (isset($values['services'])) {
      $this->services = array_intersect_key($this->services, $values['services']);
    }

    $options = $this->entityTypeManager->getStorage('cargonizer_profile')->loadMultiple();
    $options = array_map(function (CargonizerProfileInterface $profile) {
      return $profile->status() ? $profile->label() : $this->t('@label (disabled)', ['@label' => $profile->label()]);
    }, $options);
    if (count($options) === 1) {
      $form['cargonizer_profile'] = [
        '#type' => 'value',
        '#value' => key($options),
      ];
    }
    else {
      $form['cargonizer_profile'] = [
        '#type' => 'select',
        '#default_value' => $this->cargonizerProfile instanceof CargonizerProfileInterface ? $this->cargonizerProfile->id() : NULL,
        '#title' => $this->t('Cargonizer Profile'),
        '#required' => TRUE,
        '#descriptiob' => $this->t('You must create a Cargonizer profile first'),
        '#options' => $options,
        '#weight' => -99,
      ];
    }

    $form['carrier_services'] = [
      '#weight' => 99,
      '#type' => 'checkboxes',
      '#title' => t('Carrier services'),
      '#options' => $this->carrierServices,
      '#default_value' => $this->configuration['carrier_services'],
    ];

    $form['rate_overrides'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Rate overrides'),
      '#description' => $this->t('Following fields can be used to customize the amount and label for each of Carrier\'s Products'),
    ];
    foreach ($this->services as $service) {
      $form['rate_overrides'][$service->getId()] = [
        '#type' => 'fieldset',
        '#title' => $service->getLabel(),
        '#states' => [
          'visible' => [
            ':input[name="plugin[0][target_plugin_configuration][' . $this->pluginId . '][services][' . $service->getId() . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $amount = $this->configuration['rate_overrides'][$service->getId()]['rate_amount'] ?? NULL;
      $label = $this->configuration['rate_overrides'][$service->getId()]['rate_label'] ?? NULL;
      $description = $this->configuration['rate_overrides'][$service->getId()]['rate_description'] ?? NULL;
      // A bug in the plugin_select form element causes $amount to be incomplete.
      if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
        $amount = NULL;
      }
      $form['rate_overrides'][$service->getId()]['rate_label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Rate label'),
        '#default_value' => $label,
        '#attributes' => [
          'placeholder' => $service->getLabel(),
        ],
        '#description' => $this->t('Leave blank to use default Service value.'),
      ];
      $form['rate_overrides'][$service->getId()]['rate_amount'] = [
        '#type' => 'commerce_price',
        '#title' => $this->t('Flat rate amount'),
        '#default_value' => $amount,
        '#description' => $this->t('Leaving this field blank may cause that the corresponding Service will not be available in the checkout'),
      ];
      $form['rate_overrides'][$service->getId()]['rate_description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Rate description'),
        '#description' => $this->t('Provides additional details about the rate to the customer.'),
        '#default_value' => $description,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['cargonizer_profile'] = $values['cargonizer_profile'];
      $this->configuration['rate_overrides'] = $values['rate_overrides'];
      $this->configuration['carrier_services'] = $values['carrier_services'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getServices() {
    $services = parent::getServices();
    foreach ($services as $id => $service) {
      if (!empty($this->configuration['rate_overrides'][$id]['rate_label'])) {
        $services[$id] = new ShippingService($id, $this->configuration['rate_overrides'][$id]['rate_label']);
      }
    }
    return $services;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $rates = [];
    // Skip if Cargonizer profile doesn't exist.
    if (!$this->cargonizerProfile instanceof CargonizerProfileInterface) {
      return $rates;
    }
    // Skip if Cargonizer profile is disabled.
    if (!$this->cargonizerProfile->status()) {
      return $rates;
    }
    // TODO Dynamically fetch rates from API if possible.
    foreach ($this->getServices() as $service) {
      try {
        $amount = Price::fromArray($this->configuration['rate_overrides'][$service->getId()]['rate_amount'] ?? []);
      }
      catch (\Exception $e) {
        // Skip if fails.
        continue;
      }
      $rates[] = new ShippingRate([
        'shipping_method_id' => $this->parentEntity->id(),
        'service' => $service,
        'amount' => $amount,
        'description' => $this->configuration['rate_overrides'][$service->getId()]['rate_description'] ?? '',
      ]);
    }
    return $rates;
  }

  /**
   * {@inheritdoc}
   */
  public function selectRate(ShipmentInterface $shipment, ShippingRate $rate) {
    parent::selectRate($shipment, $rate);
    /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shippingMethod */
    $shipment->setData('cargonizer_mode', $this->cargonizerProfile->get('mode'));
    $shipment->setData('cargonizer_api_key', $this->cargonizerProfile->get('api_key'));
    $shipment->setData('cargonizer_sender_id', $this->cargonizerProfile->get('sender_id'));
    $shipment->setData('cargonizer_transport_agreement_id', $this->cargonizerProfile->get('transport_agreement_id'));
    $shipment->setData('cargonizer_printer_id', $this->cargonizerProfile->get('printer_id'));
  }

  /**
   * {@inheritdoc}
   */
  public function printLabel(ShipmentInterface $shipment) {
    try {
      $this->cargonizerAPI->printConsignment($shipment);
      return TRUE;
    }
    catch (\Exception $exception) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function downloadLabel(ShipmentInterface $shipment) {
    $pdf_resource_url = $shipment->getData('cargonizer_consignment_pdf');
    $response = $this->httpClient->get($pdf_resource_url, [
      'headers' => [
        'X-Cargonizer-Key' => $shipment->getData('cargonizer_api_key'),
        'X-Cargonizer-Sender' => $shipment->getData('cargonizer_sender_id'),
      ],
    ]);
    $pdf_content = $response->getBody()->getContents();
    $file_response = new Response($pdf_content);
    $file_name = sprintf('Labels_%s.pdf', date('Ymd-Hi'));
    $disposition = $file_response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file_name);
    $file_response->headers->set('Content-type', 'application/pdf');
    $file_response->headers->set('Content-Disposition', $disposition);
    $file_response->headers->set('Content-length', strlen($pdf_content));
    return $file_response;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $code = $shipment->getTrackingCode();
    if (!empty($code)) {
      // Otherwise, append the tracking code to the end of the URL.
      $url = sprintf(static::TRACKING_URL, $code);
      return Url::fromUri($url);
    }
    return FALSE;
  }

}
