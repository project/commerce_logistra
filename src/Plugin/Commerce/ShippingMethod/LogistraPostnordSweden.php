<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Postnord Sweden products.
 *
 * @CommerceShippingMethod(
 *  id = "commerce_logistra_postnord_sweden",
 *  label = @Translation("Postnord Sweden (Logistra)"),
 *  services = {
 *    "postnord_foretagspaket_special" = "DPD Företagspaket Special",
 *    "postnord_groupage" = "Groupage",
 *    "postnord_international_parcel" = "International parcel",
 *    "postnord_mypack" = "MyPack Collect",
 *    "postnord_mypack_home" = "Mypack Home",
 *    "postnord_pall_ett" = "Pallett",
 *    "postnord_foretagspaket_09" = "Parcel 09",
 *    "postnord_foretagspaket_12" = "Parcel 12",
 *    "postnord_foretagspaket_16" = "Parcel",
 *    "postnord_mypack_return" = "Return Drop Off",
 *    "postnord_varubrev_inrikes_1" = "Varubrev Inrikes 1:a Klass",
 *    "postnord_varubrev_inrikes_ekonomi" = "Varubrev Inrikes Ekonomi",
 *    "postnord_varubrev_utrikes_1" = "Varubrev Utrikes 1:a Klass",
 *    "postnord_varubrev_utrikes_ekonomi" = "Varubrev Utrikes Ekonomi",
 *  }
 * )
 */
class LogistraPostnordSweden extends LogistraBase implements SupportsTrackingInterface {

  /**
   * Adding Tracking URL pattern here which is utilized in base class.
   *
   * @see \Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod\LogistraBase::getTrackingUrl()
   */
  const TRACKING_URL = "https://my.postnord.no/tracking/%s";

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager, $entity_type_manager);
    $this->carrierServices = [
      "postnord_delivery_without_pod" => "Delivery without POD",
      "postnord_flexchange" => "Flex change",
      "postnord_notification_email" => "Notification By Email",
      "postnord_notification_letter" => "Notification By Letter",
      "postnord_notification_sms" => "Notification By SMS",
      "postnord_optional_servicepoint" => "Optional servicepoint",
      "postnord_private_receiver" => "Private receiver",
      "postnord_leveransanvisning" => "Time Agreed Delivery",
    ];
  }

}
