<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_logistra\CargonizerAPI;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Legacy Bring products.
 *
 * For new products (starting with bring2_ prefix) use LogistraBring.
 *
 * @CommerceShippingMethod(
 *  id = "commerce_logistra_bring_legacy",
 *  label = @Translation("Bring Legacy (Logistra)"),
 *  services = {
 *     "bring_bedr_dor_dor" = "Bedriftspakke",
 *     "bring_bedr_flerkolli" = "Bedriftspakke Flerkolli",
 *     "bring_express_business_distribution" = "Business Distribution",
 *     "bring_express_business_distribution_pallet" = "Business Distribution Pallet",
 *     "bring_carryon_business_pallet" = "Business Pallet",
 *     "bring_carryon_business" = "Business Parcel",
 *     "bring_carryon_business_bulksplit" = "Business Parcel Bulk",
 *     "bring_business_parcel_return" = "Business Parcel Return",
 *     "bring_business_parcel_bulk_return" = "Business Parcel Return Bulk",
 *     "bring_express_courier_distribution" = "Courier Distribution",
 *     "bring_express_courier_distribution_pallet" = "Courier Distribution Pallet",
 *     "bring_express_courier_service_bicycle" = "Courier Service by bicycle",
 *     "bring_express_courier_service_car" = "Courier Service by car",
 *     "bring_bedr_exp_natt_9" = "Ekspress over natten",
 *     "bring_flyfrakt" = "Flyfrakt",
 *     "bring_flyfrakt_utland" = "Flyfrakt Utland",
 *     "bring_pa_doren" = "Hjemlevering",
 *     "bring_home_delivery_curbside" = "Home Delivery Curbside",
 *     "bring_home_delivery_double_indoor" = "Home Delivery Double indoor",
 *     "bring_carryon_home_shopping_bulksplit_home" = "Home Delivery Parcel",
 *     "bring_home_delivery_single_indoor" = "Home Delivery Single Indoor",
 *     "bring_servicepakke" = "Klimanøytral Servicepakke",
 *     "bring_sunnmore" = "Møbeltransport",
 *     "bring_small_parcel_a_no_rfid" = "Pakke i postkassen",
 *     "bring_small_parcel_a" = "Pakke i postkassen med sporing",
 *     "bring_pallelaster" = "Pallelaster",
 *     "bring_partigods" = "Partifrakt",
 *     "bring_pickup_point" = "Pick Up Point",
 *     "bring_carryon_home_shopping" = "PickUp Parcel",
 *     "bring_carryon_home_shopping_bulksplit" = "PickUp Parcel Bulk",
 *     "bring_pickup_parcel_return" = "PickUp Parcel Return",
 *     "bring_pickup_parcel_bulk_return" = "PickUp Parcel Return Bulk",
 *     "bring_pum" = "PUM (Personlig utlevering mottakingsbevis)",
 *     "bring_pose_pa_doren_no_rfid" = "Pose på døren",
 *     "bring_pose_pa_doren_rfid" = "Pose på døren med sporing",
 *     "bring_rek" = "Rekommandert brev",
 *     "bring_rek_pp" = "Rekommandert brev (PP)",
 *     "bring_rek_utl" = "Rekommandert brev utland",
 *     "bring_rek_utl_pp" = "Rekommandert brev utland (PP)",
 *     "bring_home_delivery_return" = "Return Home Delivery",
 *     "bring_bedriftspakke_return" = "Returservice Bedriftspakke",
 *     "bring_bedriftspakke_ekspress_return" = "Returservice Bedriftspakke Ekspress",
 *     "bring_servicepakke_return" = "Returservice Klimanøytral Servicepakke",
 *     "bring_sjofrakt" = "Sjøfrakt",
 *     "bring_sjofrakt_utland" = "Sjøfrakt Utland",
 *     "bring_express_store_home_delivery_curbside" = "Store Home Delivery Curbside",
 *     "bring_express_store_home_delivery_indoor" = "Store Home Delivery Indoor",
 *     "bring_express_store_home_delivery_indoor_light" = "Store Home Delivery Indoor Light",
 *     "bring_stykk_parti" = "Stykk- partigods utland",
 *     "bring_stykkgods" = "Stykkgods",
 *     "bring_stykkgods_booking_other" = "Stykkgods Hent Lever",
 *     "bring_stykkgods_booking_me" = "Stykkgods Retur",
 *     "bring_express_technical_distribution" = "Technical Distribution",
 *  }
 * )
 */
class LogistraBringLegacy extends LogistraBase implements SupportsTrackingInterface {

  /**
   * Adding Tracking URL pattern here which is utilized in base class.
   *
   * @see \Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod\LogistraBase::getTrackingUrl()
   */
  const TRACKING_URL = "https://sporing.bring.no/sporing/%s";

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager, $entity_type_manager);
    $this->carrierServices = [
      'bring_delivery_attempt_1' => '1 Utlev. forsøk',
      'bring_1_hour' => '1 hour',
      'bring_1_2_pallet' => '1/2 Pallet',
      'bring_delivery_attempt_2' => '2 Utlev. forsøk',
      'bring_2_hour' => '2 hours',
      'bring_4_hour' => '4 hours',
      'bring_advisering' => 'Advisering',
      'bring_e_varsle_for_utlevering' => 'eVarsling (Parcels)',
      'bring_e_varsle_mottaker_om_distrib' => 'eVarsling (Cargo)',
      'bring_flex_delivery' => 'Flex Delivery',
      'bring_forenklet_utlevering' => 'Forenklet utlevering',
      'bring_format_maxi' => 'Format Maxi',
      'bring_format_small' => 'Format Små',
      'bring_format_large' => 'Format Store',
      'bring_insurance_single_item' => 'Forsikring Kolli',
      'bring_id_kontrakt' => 'ID/Kontrakt',
      'bring_installation_standard' => 'Installation Std',
      'bring_passport' => 'Krav til pass',
      'bring_legitimasjonsplikt' => 'Legitimasjonsplikt',
      'bring_delivery_time' => 'Leveringstidspunkt',
      'bring_long_distance' => 'Long Distance',
      'bring_personlig_utlevering' => 'Personlig utlevering',
      'bring_oppkrav' => 'Postoppkrav',
      'bring_priority' => 'Priority',
      'bring_recycling_standard' => 'Recycling Std',
      'bring_return_wrapping' => 'Retur emballasje',
      'bring_returservice' => 'Returservice',
      'bring_signature_required' => 'Signature required',
      'bring_sms_advisering' => 'SMS Advisering',
      'bring_sosial_kontroll' => 'Sosial kontroll',
      'bring_spesialgods' => 'Spesialgods',
      'bring_swap' => 'Swap',
      'bring_log_temperaturregulert' => 'Temp.regulert gods',
      'bring_temperatur' => 'Temperatur',
      'bring_tidlig_distribusjon' => 'Tidlig distribusjon',
      'bring_utkjoring_lordag' => 'Utkjøring Lørdag',
      'bring_utvekslingspaller' => 'Utvekslingspaller',
      'bring_vip' => 'VIP',
      'bring_valgfritt_postkontor' => 'Valgfritt hentested',
    ];
  }

}
