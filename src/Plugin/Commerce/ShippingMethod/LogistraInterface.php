<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface;

/**
 *
 */
interface LogistraInterface extends ShippingMethodInterface {

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return bool
   */
  public function printLabel(ShipmentInterface $shipment);

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return
   */
  public function downloadLabel(ShipmentInterface $shipment);

}
