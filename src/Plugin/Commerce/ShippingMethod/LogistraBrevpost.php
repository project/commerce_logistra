<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_logistra\CargonizerAPI;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * @CommerceShippingMethod(
 *  id = "commerce_logistra_brevpost",
 *  label = @Translation("Brevpost (Logistra)"),
 *  services = {
 *    "brevpost-a" = "Brevpost",
 *    "brevpost-a-franko" = "Brevpost PP",
 *    "brevpost-toll" = "Brev m/tolldeklarasjon",
 *  }
 * )
 */
class LogistraBrevpost extends LogistraBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager, $entity_type_manager);
    $this->carrierServices = [
      'customs_declaration' => $this->t('Customs declaration'),
      'digital_stamp' => $this->t('Digital stamp'),
    ];
  }

}
