<?php

namespace Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Postnord Norway products.
 *
 * @CommerceShippingMethod(
 *  id = "commerce_logistra_postnord_norway",
 *  label = @Translation("Postnord Norway (Logistra)"),
 *  services = {
 *    "tg_dpd_utland" = "DPD - Export",
 *    "postnord_express_next_day" = "Express Next Day",
 *    "tg_stykkgods" = "Groupage",
 *    "postnord_stykkgods_utland" = "Groupage Export",
 *    "stykkgods_innland_booking_other" = "Groupage Pickup-Deliv.",
 *    "stykkgods_innland_booking_me" = "Groupage Return",
 *    "varebrev_split" = "Home Small Hovedsending",
 *    "postnord_innight" = "InNight",
 *    "mypack" = "MyPack Collect",
 *    "postnord_mypack_eksport" = "MyPack Collect Export",
 *    "tg_home_delivery" = "Mypack Home",
 *    "postnord_mypack_home" = "MyPack Home 0-35kg",
 *    "postnord_mypack_home_export" = "Mypack Home Intl.",
 *    "postnord_home_nordic" = "Mypack Home Nordic",
 *    "postnord_mypack_home_small" = "Mypack Home Small",
 *    "tg_pall" = "Pallet",
 *    "postnord_pall_eksport" = "Pallet Export",
 *    "postnord_parcel_nordic" = "Parcel",
 *    "tg_parti" = "Partloads",
 *    "mypack_return" = "Return Drop Off",
 *    "postnord_mypack_eksport_return" = "Return Drop Off Export",
 *    "postnord_local_courier" = "Bud",
 *    "postnord_local_express" = "Express",
 *    "postnord_local_truck" = "Lastebil",
 *    "postnord_local_goods" = "Vare",
 *    "postnord_local_vip" = "VIP",
 *    "postnord_local_economy" = "Økonomi Rute",
 *  }
 * )
 */
class LogistraPostnordNorway extends LogistraBase implements SupportsTrackingInterface {

  /**
   * Adding Tracking URL pattern here which is utilized in base class.
   *
   * @see \Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod\LogistraBase::getTrackingUrl()
   */
  const TRACKING_URL = "https://my.postnord.no/tracking/%s";

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager, $entity_type_manager);
    $this->carrierServices = [
      "postnord_timeagreed_delivery" => "Avtalt lev. tidspunkt",
      "postnord_before_12" => "Before 12",
      "postnord_before_9" => "Before 9",
      "postnord_limited_dgs" => "Begrenset mengde FG",
      "postnord_carry_in" => "Carry In",
      "postnord_delivery_advice" => "Delivery Advice",
      "postnord_delivery_confirmation" => "Delivery Confirmation",
      "postnord_delivery_notification" => "Delivery Notifcation",
      "postnord_notification_email" => "E-post Varsling",
      "postnord_ee_retur" => "EE Retur",
      "tg_return_packaging" => "Emballasje retur",
      "cod" => "Etterkrav",
      "tg_etterkrav" => "Etterkrav",
      "tg_insurance" => "Forsikring",
      "tg_hjemlevering" => "Hjemlevering",
      "postnord_id_check" => "ID Check",
      "postnord_innlevering_butikk" => "Innlevering butikk",
      "postnord_indoor_delivery" => "Leveres innendørs",
      "postnord_notification_driver" => "Notification by driver",
      "postnord_optional_servicepoint" => "Optional Servicepoint",
      "postnord_pose_pa_doren" => "Pose på døren",
      "postnord_private_receiver" => "Private Receiver",
      "postnord_notification_by_telephone" => "Ring før levering",
      "postnord_notification_sms" => "SMS Varsling",
      "postnord_split_at_destination" => "Split at destination",
      "postnord_produce_label" => "Transportør skriver ut etikett",
      "postnord_delivery_without_receipt" => "Utleveres uten sign. (aka Unattended)",
      "tg_giro" => "Utleveringsforbehold",
      "tg_heat" => "Varme",
    ];
  }

}
