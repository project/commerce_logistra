<?php

namespace Drupal\commerce_logistra\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Cargonizer Profile entity type.
 */
interface CargonizerProfileInterface extends ConfigEntityInterface {

}
