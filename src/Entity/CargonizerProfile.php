<?php

namespace Drupal\commerce_logistra\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Cargonizer Profile entity type.
 *
 * @ConfigEntityType(
 *   id = "cargonizer_profile",
 *   label = @Translation("Cargonizer Profile"),
 *   label_collection = @Translation("Cargonizer Profiles"),
 *   label_singular = @Translation("Cargonizer Profile"),
 *   label_plural = @Translation("Cargonizer Profiles"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Cargonizer Profile",
 *     plural = "@count Cargonizer Profiles",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\commerce_logistra\CargonizerProfileListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_logistra\Form\CargonizerProfileForm",
 *       "edit" = "Drupal\commerce_logistra\Form\CargonizerProfileForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "cargonizer_profile",
 *   admin_permission = "administer cargonizer_profile",
 *   links = {
 *     "collection" = "/admin/commerce/config/shipping/cargonizer-profile",
 *     "add-form" = "/admin/commerce/config/shipping/cargonizer-profile/add",
 *     "edit-form" = "/admin/commerce/config/shipping/cargonizer-profile/{cargonizer_profile}",
 *     "delete-form" = "/admin/commerce/config/shipping/cargonizer-profile/{cargonizer_profile}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "mode",
 *     "api_key",
 *     "sender_id",
 *     "transport_agreement_id",
 *     "printer_id",
 *     "return_address",
 *     "consignor_reference",
 *     "consignee_reference",
 *     "create_consignment_on_finalize",
 *     "print_on_finalize",
 *     "create_consignment_on_ship",
 *     "print_on_ship"
 *   }
 * )
 */
class CargonizerProfile extends ConfigEntityBase implements CargonizerProfileInterface {

  /**
   * The Cargonizer Profile ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Cargonizer Profile label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Cargonizer Profile status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The cargonizer_profile description.
   *
   * @var string
   */
  protected $description;

}
