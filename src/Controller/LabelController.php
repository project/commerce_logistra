<?php

namespace Drupal\commerce_logistra\Controller;

use Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod\LogistraInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;

/**
 * Allows downloading the PDF label.
 */
class LabelController extends ControllerBase {

  /**
   * Downloads label by proxying it from the API.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The commerce shipment.
   */
  public function downloadLabel(ShipmentInterface $commerce_shipment) {
    if (($plugin = $commerce_shipment->getShippingMethod()->getPlugin()) && $plugin instanceof LogistraInterface) {
      return $plugin->downloadLabel($commerce_shipment);
    }
  }

  /**
   * The access callback.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The commerce shipment.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   The access result.
   */
  public function access(ShipmentInterface $commerce_shipment) {
    if (!$commerce_shipment->getShippingMethod() instanceof ShippingMethodInterface) {
      return AccessResult::forbidden('Shipping method is missing');
    }
    $api_key = $commerce_shipment->getData('cargonizer_api_key');
    $sender_id = $commerce_shipment->getData('cargonizer_sender_id');
    $pdf_resource_url = $commerce_shipment->getData('cargonizer_consignment_pdf');
    $plugin = $commerce_shipment->getShippingMethod()->getPlugin();
    if (empty($api_key) || empty($sender_id) || empty($pdf_resource_url) || !$plugin instanceof LogistraInterface) {
      return AccessResult::forbidden('Missing required parameters to download the PDF');
    }
    return AccessResult::allowed();
  }

}
