<?php

namespace Drupal\commerce_logistra\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Component\EventDispatcher\Event;
use zaporylie\Cargonizer\Data\Consignments;

/**
 * Defines the create consignment event.
 */
class CreateConsignmentsEvent extends Event {

  /**
   * The consignments.
   *
   * @var \zaporylie\Cargonizer\Data\Consignments
   */
  protected $consignments;

  /**
   * The shipment.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * Constructs an event object.
   *
   * @param \zaporylie\Cargonizer\Data\Consignments $consignments
   *   The consignmnets.
   */
  public function __construct(Consignments $consignments) {
    $this->consignments = $consignments;
  }

  /**
   * Gets consignments value.
   *
   * @return \zaporylie\Cargonizer\Data\Consignments
   *   The consignments.
   */
  public function getConsignments(): Consignments {
    return $this->consignments;
  }

  /**
   * Sets shipment variable.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return $this
   */
  public function setShipment(ShipmentInterface $shipment) {
    $this->shipment = $shipment;
    return $this;
  }

  /**
   * Get shipment.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface|null
   *   The shipment.
   */
  public function getShipment() {
    return $this->shipment ?? NULL;
  }

}
