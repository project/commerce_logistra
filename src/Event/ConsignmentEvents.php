<?php

namespace Drupal\commerce_logistra\Event;

/**
 * Defines events around consignment handling.
 */
final class ConsignmentEvents {

  /**
   * Name of the event fired when creating new consignment.
   *
   * @Event
   *
   * @see \Drupal\commerce_logistra\Event\CreateConsignmentEvent
   */
  const CREATE = 'commerce_logistra.create_consignment';

}
