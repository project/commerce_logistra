<?php

namespace Drupal\commerce_logistra;

use Drupal\commerce_logistra\Entity\CargonizerProfileInterface;
use Drupal\commerce_logistra\Event\ConsignmentEvents;
use Drupal\commerce_logistra\Event\CreateConsignmentsEvent;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drupal\profile\Entity\ProfileInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use zaporylie\Cargonizer\Agreements;
use zaporylie\Cargonizer\Config;
use zaporylie\Cargonizer\Consignment as ConsignmentService;
use zaporylie\Cargonizer\ConsignmentPrinter as ConsignmentPrinterService;
use zaporylie\Cargonizer\Data\Consignee;
use zaporylie\Cargonizer\Data\Consignment;
use zaporylie\Cargonizer\Data\Consignments;
use zaporylie\Cargonizer\Data\Item;
use zaporylie\Cargonizer\Data\Items;
use zaporylie\Cargonizer\Data\Parts;
use zaporylie\Cargonizer\Data\References;
use zaporylie\Cargonizer\Data\ReturnAddress;
use zaporylie\Cargonizer\Data\ServicePartner;

/**
 * CargonizerAPI service.
 */
class CargonizerAPI {

  use StringTranslationTrait;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructs a CargonizerAPI object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Utility\Token $token
   *   The token.
   */
  public function __construct(LoggerInterface $logger, EventDispatcherInterface $event_dispatcher, Token $token) {
    $this->logger = $logger;
    $this->eventDispatcher = $event_dispatcher;
    $this->token = $token;
  }

  /**
   * Gets all agreements.
   */
  public function getAgreements(CargonizerProfileInterface $cargonizerProfile) {
    // @todo: Cache.
    Config::set('endpoint', $cargonizerProfile->get('mode') === 'live' ? Config::PRODUCTION : Config::SANDBOX);
    Config::set('secret', $cargonizerProfile->get('api_key'));
    Config::set('sender', $cargonizerProfile->get('sender_id'));

    return (new Agreements())->getAgreements();
  }

  /**
   * Gets agreement for Shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return false|\zaporylie\Cargonizer\Data\TransportAgreement|null
   */
  public function getAgreementForShipment(ShipmentInterface $shipment) {
    Config::set('endpoint', $shipment->getData('cargonizer_mode') === 'live' ? Config::PRODUCTION : Config::SANDBOX);
    Config::set('secret', $shipment->getData('cargonizer_api_key'));
    Config::set('sender', $shipment->getData('cargonizer_sender_id'));

    $agreements = (new Agreements())->getAgreements();
    /** @var \zaporylie\Cargonizer\Data\TransportAgreement $agreement */
    foreach ($agreements as $agreement) {
      if ($agreement->getId() == $shipment->getData('cargonizer_transport_agreement_id')) {
        return $agreement;
      }
    }
    return NULL;
  }

  /**
   * Gets list of pickup points.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param string $carrier_id
   *   If the name of the carrier is well known, ex. bring2, you can provide it
   *   directly.
   *
   * @return \zaporylie\Cargonizer\Data\ServicePartners
   */
  public function getPickupPoints(ShipmentInterface $shipment, $carrier_id = '') {
    // In order to mitigate the need for extra API call to obtain otherwise
    // static carrier ID we allow to provide it as an optional argument.
    if (empty($carrier_id)) {
      $agreement = $this->getAgreementForShipment($shipment);
      $carrier_id = $agreement->getCarrier()->getIdentifier();
    }
    Config::set('endpoint', $shipment->getData('cargonizer_mode') === 'live' ? Config::PRODUCTION : Config::SANDBOX);
    Config::set('secret', $shipment->getData('cargonizer_api_key'));
    Config::set('sender', $shipment->getData('cargonizer_sender_id'));
    /** @var \zaporylie\Cargonizer\Data\Results $result */
    $result = (new \zaporylie\Cargonizer\Partners())->getPickupPoints($shipment->getShippingProfile()->address->country_code, $shipment->getShippingProfile()->address->postal_code, $carrier_id);
    return $result->getServicePartners();
  }

  /**
   * Gets estimate.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \zaporylie\Cargonizer\Data\Estimation
   */
  public function getEstimation(ShipmentInterface $shipment) {
    Config::set('endpoint', $shipment->getData('cargonizer_mode') === 'live' ? Config::PRODUCTION : Config::SANDBOX);
    Config::set('secret', $shipment->getData('cargonizer_api_key'));
    Config::set('sender', $shipment->getData('cargonizer_sender_id'));
    // Estimate cost.
    $item = (new Item())
      ->setPackage('package')
      ->setAmount(1)
      ->setWeight($shipment->getWeight()->getNumber());

    $items = (new Items())
      ->addItem($item);

    $consignee = (new Consignee());
    if ($shipment->getShippingProfile() instanceof ProfileInterface) {
      $consignee
        ->setCountry($shipment->getShippingProfile()->address->country_code)
        ->setPostcode($shipment->getShippingProfile()->address->postal_code)
        ->setName($shipment->getShippingProfile()->address->name);
    }

    $parts = (new Parts())
      ->setConsignee($consignee);

    $referebces = (new References());

    $consignment = (new Consignment())
      ->setTransportAgreement($shipment->getData('cargonizer_transport_agreement_id'))
      ->setProduct($shipment->getShippingService())
      ->setParts($parts)
      ->setItems($items);
    $consignment->setEstimate(TRUE);
    $consignment->setReferences($referebces);

    $consignments = (new Consignments())->addItem($consignment);
    return (new ConsignmentPrinterService())->getEstimation($consignments);
  }

  /**
   * Creates Consignment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return \zaporylie\Cargonizer\Data\ConsignmentsResponse
   */
  public function createConsignment(ShipmentInterface $shipment) {
    Config::set('endpoint', $shipment->getData('cargonizer_mode') === 'live' ? Config::PRODUCTION : Config::SANDBOX);
    Config::set('secret', $shipment->getData('cargonizer_api_key'));
    Config::set('sender', $shipment->getData('cargonizer_sender_id'));
    // Estimate cost.
    $item = (new Item())
      ->setPackage('package')
      ->setAmount(1)
      ->setDescription($this->getShipmentDescription($shipment))
      ->setWeight((float) $shipment->getWeight()->getNumber());

    $items = (new Items())
      ->addItem($item);

    $consignee = (new Consignee());
    if ($shipment->getShippingProfile() instanceof ProfileInterface) {
      $consignee
        ->setCustomerNumber($shipment->getOrder()->getCustomerId())
        ->setEmail($shipment->getOrder()->getEmail())
        ->setAddress1($shipment->getShippingProfile()->address->address_line1)
        ->setAddress2($shipment->getShippingProfile()->address->address_line2)
        ->setCountry($shipment->getShippingProfile()->address->country_code)
        ->setCity($shipment->getShippingProfile()->address->locality)
        ->setPostcode($shipment->getShippingProfile()->address->postal_code)
        ->setName(sprintf('%s %s', $shipment->getShippingProfile()->address->given_name, $shipment->getShippingProfile()->address->family_name));
    }

    $parts = (new Parts())
      ->setConsignee($consignee);

    $references = (new References());

    // If shipment's cargonizer profile has a return address - use it.
    $shipping_method_configuration = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $cargonizerProfile = \Drupal::entityTypeManager()->getStorage('cargonizer_profile')->load($shipping_method_configuration['cargonizer_profile'] ?? NULL);
    if ($cargonizerProfile instanceof CargonizerProfileInterface) {
      $return_address = $cargonizerProfile->get('return_address');
      $return_address_part = new ReturnAddress();
      $return_address_part->setAddress1($return_address['address_line1'] ?? NULL);
      $return_address_part->setAddress2($return_address['address_line2'] ?? NULL);
      $return_address_part->setCity($return_address['locality'] ?? NULL);
      $return_address_part->setCountry($return_address['country_code'] ?? NULL);
      $return_address_part->setPostcode($return_address['postal_code'] ?? NULL);
      $return_address_part->setName(empty($return_address['organization']) ? sprintf('%s %s', $return_address['given_name'] ?? '', $return_address['family_name'] ?? '') : $return_address['organization']);
      $parts->setReturnAddress($return_address_part);
      $references->addConsignorReference($this->token->replacePlain($cargonizerProfile->get('consignor_reference') ?? '', [
        'commerce_shipment' => $shipment,
        'commerce_order' => $shipment->getOrder(),
      ]));
      $references->addConsigneeReference($this->token->replacePlain($cargonizerProfile->get('consignee_reference') ?? '', [
        'commerce_shipment' => $shipment,
        'commerce_order' => $shipment->getOrder(),
      ]));
    }

    $consignment = (new Consignment())
      ->setTransportAgreement($shipment->getData('cargonizer_transport_agreement_id'))
      ->setProduct($shipment->getShippingService())
      ->setParts($parts)
      ->setItems($items);

    $services = !empty($shipping_method_configuration['carrier_services']) ? array_values(array_filter($shipping_method_configuration['carrier_services'])) : [];
    $consignment->setServices($services);

    $consignment->setTransfer(TRUE);
    $consignment->setReferences($references);
    $consignment->addValue('order_number', $shipment->getOrder()->getOrderNumber());

    $consignments = (new Consignments())->addItem($consignment);

    $event = new CreateConsignmentsEvent($consignments);
    $event->setShipment($shipment);
    $this->eventDispatcher->dispatch($event, ConsignmentEvents::CREATE);
    return (new ConsignmentService())->createConsignments($consignments);
  }

  /**
   * Prints consignment on a printer.
   *
   * If only shipment is provided the consignment stored on the shipment will be
   * printed on the printer id also stored on the shipment. You can provide
   * alternative printer id if you want to use specific printer, and provide
   * alternative consignment ID if you want to print some another consignment,
   * ex. return consignment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param string|null $printer_id
   *   The printer ID, NULL to use default.
   * @param string|null $consignment_id
   *   The consignment ID, NULL to use default.
   *
   * @return mixed
   */
  public function printConsignment(ShipmentInterface $shipment, $printer_id = NULL, $consignment_id = NULL) {
    Config::set('endpoint', $shipment->getData('cargonizer_mode') === 'live' ? Config::PRODUCTION : Config::SANDBOX);
    Config::set('secret', $shipment->getData('cargonizer_api_key'));
    Config::set('sender', $shipment->getData('cargonizer_sender_id'));
    return (new ConsignmentPrinterService())->printConsigment($consignment_id ?? $shipment->getData('cargonizer_consignment_id'), $printer_id ?? $shipment->getData('cargonizer_printer_id'));
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return string
   */
  protected function getShipmentDescription(ShipmentInterface $shipment) {
    // @todo: Make it vary a bit based on stuff.
    $description = $this->t('Order #@number', ['@number' => $shipment->getOrder()->getOrderNumber()]);
    return $description;
  }

}
