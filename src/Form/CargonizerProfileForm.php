<?php

namespace Drupal\commerce_logistra\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Cargonizer Profile form.
 *
 * @property \Drupal\commerce_logistra\CargonizerProfileInterface $entity
 */
class CargonizerProfileForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the Cargonizer Profile.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_logistra\Entity\CargonizerProfile::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the Cargonizer Profile.'),
    ];

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Enabled'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $this->entity->get('mode'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->entity->get('api_key'),
      '#description' => $this->t('The API Key'),
      '#required' => TRUE,
    ];

    $form['sender_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender ID'),
      '#default_value' => $this->entity->get('sender_id'),
      '#description' => $this->t('The Sender ID'),
      '#required' => TRUE,
    ];

    $form['transport_agreement_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The transport agreement ID'),
      '#default_value' => $this->entity->get('transport_agreement_id'),
      '#description' => $this->t('The Transport Agreement ID'),
      '#required' => TRUE,
    ];

    $form['printer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Printer ID'),
      '#default_value' => $this->entity->get('printer_id'),
      '#description' => $this->t('The Printer ID'),
    ];

    $form['return_address_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Return Address'),
    ];
    $form['return_address_container']['return_address'] = [
      '#type' => 'address',
      '#title' => $this->t('Return Address'),
      '#default_value' => $this->entity->get('return_address'),
      '#description' => $this->t('The return address to be included in Consignment'),
    ];

    $form['consignor_reference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consignor Reference'),
      '#default_value' => $this->entity->get('consignor_reference'),
      '#maxlength' => 1024,
    ];

    $form['consignee_reference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consignee Reference'),
      '#default_value' => $this->entity->get('consignee_reference'),
      '#maxlength' => 1024,
    ];
    $form['token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => array('commerce_shipment', 'commerce_order'),
    );

    $form['create_consignment_on_finalize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create Consignmnet on Finalize'),
      '#default_value' => $this->entity->get('create_consignment_on_finalize'),
    ];

    $form['print_on_finalize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Print On Finalize'),
      '#default_value' => $this->entity->get('print_on_finalize'),
      '#states' => [
        'visible' => [
          ':input[name="create_consignment_on_finalize"]' => ['checked' => TRUE],
          'and',
          ':input[name="printer_id"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['create_consignment_on_ship'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create Consignmnet on Ship'),
      '#default_value' => $this->entity->get('create_consignment_on_ship'),
    ];

    $form['print_on_ship'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Print On Ship'),
      '#default_value' => $this->entity->get('print_on_ship'),
      '#states' => [
        'enabled' => [
          ':input[name="create_consignment_on_ship"]' => ['checked' => TRUE],
          'and',
          ':input[name="printer_id"]' => ['!value' => ''],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new Cargonizer Profile %label.', $message_args)
      : $this->t('Updated Cargonizer Profile %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
