<?php

namespace Drupal\commerce_logistra\EventSubscriber;

use Drupal\commerce_logistra\CargonizerAPI;
use Drupal\commerce_logistra\Entity\CargonizerProfileInterface;
use Drupal\commerce_logistra\Plugin\Commerce\ShippingMethod\LogistraInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Takes care of reacting to shipment transition.
 */
class ShipmentTransitionSubscriber implements EventSubscriberInterface {

  /**
   * The Cargonizer API service.
   *
   * @var \Drupal\commerce_logistra\CargonizerAPI
   */
  protected $cargonizerAPI;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The subscriber constructor.
   *
   * @param \Drupal\commerce_logistra\CargonizerAPI $cargonizer_api
   *   The Cargonizer API.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(CargonizerAPI $cargonizer_api, EntityTypeManagerInterface $entity_type_manager) {
    $this->cargonizerAPI = $cargonizer_api;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_shipment.finalize.pre_transition'] = [
      ['createConsignment', 10],
      ['printConsignment'],
    ];
    $events['commerce_shipment.ship.pre_transition'] = [
      ['createConsignment', 10],
      ['printConsignment'],
    ];
    return $events;
  }

  /**
   * Creates consignment.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow event.
   */
  public function createConsignment(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $event->getEntity();
    $shippingMethodPlugin = $shipment->getShippingMethod()->getPlugin();
    // Do not consider shipments that are not made with the Logistra.
    if (!$shippingMethodPlugin instanceof LogistraInterface) {
      return;
    }
    $configuration = $shippingMethodPlugin->getConfiguration();
    // Skip if profile is missing.
    if (empty($configuration['cargonizer_profile'])) {
      return;
    }
    $cargonizerProfile = $this
      ->entityTypeManager
      ->getStorage('cargonizer_profile')
      ->load($configuration['cargonizer_profile']);
    // Skip if profile cannot be found.
    if (!$cargonizerProfile instanceof CargonizerProfileInterface) {
      return;
    }
    // Skip if profile is disabled.
    if (!$cargonizerProfile->status()) {
      return;
    }
    // Skip if this creation of consignment was disabled.
    if ($event->getTransition()->getId() === 'finalize' && !$cargonizerProfile->get('create_consignment_on_finalize')) {
      return;
    }
    elseif ($event->getTransition()->getId() === 'ship' && !$cargonizerProfile->get('create_consignment_on_ship')) {
      return;
    }
    if ($shipment->getData('cargonizer_consignment_id')) {
      // @todo: Log that consignment was already created.
      return;
    }
    try {
      $consignments = $this->cargonizerAPI->createConsignment($shipment);
      /** @var \zaporylie\Cargonizer\Data\ConsignmentResponse $consignment */
      $consignment = $consignments->current();
      $shipment->setData('cargonizer_consignment_id', $consignment->getId());
      $shipment->setData('cargonizer_consignment_pdf',
        $consignment->getConsignmentPdf());
      $shipment->setData('cargonizer_waybill_pdf',
        $consignment->getWaybillPdf());
      $shipment->setData('cargonizer_tracking_url',
        $consignment->getTrackingUrl());
      $shipment->setTrackingCode($consignment->getNumberWithChecksum());
    }
    catch (\Exception $exception) {
      // @todo: Log it and retry?
    }
  }

  /**
   * Prints consignmnet on the physical printer.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function printConsignment(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $event->getEntity();
    $shippingMethodPlugin = $shipment->getShippingMethod()->getPlugin();
    // Do not consider shipments that are not made with the Logistra.
    if (!$shippingMethodPlugin instanceof LogistraInterface) {
      return;
    }
    $configuration = $shippingMethodPlugin->getConfiguration();
    // Skip if profile is missing.
    if (empty($configuration['cargonizer_profile'])) {
      return;
    }
    $cargonizerProfile = $this
      ->entityTypeManager
      ->getStorage('cargonizer_profile')
      ->load($configuration['cargonizer_profile']);
    // Skip if profile cannot be found.
    if (!$cargonizerProfile instanceof CargonizerProfileInterface) {
      return;
    }
    // Skip if profile is disabled.
    if (!$cargonizerProfile->status()) {
      return;
    }
    // Skip if this creation of consignment was disabled.
    if ($event->getTransition()->getId() === 'finalize' && !$cargonizerProfile->get('print_on_finalize')) {
      return;
    }
    elseif ($event->getTransition()->getId() === 'ship' && !$cargonizerProfile->get('print_on_ship')) {
      return;
    }
    // Should have been created by self::createConsignment().
    if (!$shipment->getData('cargonizer_consignment_id')) {
      // @todo: Log that consignment is missing.
      return;
    }
    // You can easily skip printing by unsetting printer id on $shipment.
    if (!$shipment->getData('cargonizer_printer_id')) {
      // @todo: Log that printer ID is missing.
      return;
    }

    // Print.
    if (!$shippingMethodPlugin->printLabel($shipment)) {
      // @todo: Log that printing failed.
    }
  }

}
